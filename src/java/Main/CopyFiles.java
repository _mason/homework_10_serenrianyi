package Main;

import java.io.*;

public class CopyFiles {

    public static void main(String[] args) throws IOException {

        System.out.println("use text1 and text2");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("пустой \\text1");
        FileOutputStream fis1 = new FileOutputStream(reader.readLine());
        System.out.println("содержит данные\\text2");
        FileInputStream fis2 = new FileInputStream(reader.readLine());

        while (fis2.available() > 0)
        {
            int data = fis2.read();
            fis1.write(data);
        }
        fis2.close();
        fis1.close();
    }
}
