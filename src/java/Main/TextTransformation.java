package Main;

import java.io.*;
import java.util.Scanner;

public class TextTransformation {

    public static void main(String[] args) throws IOException {

        System.out.println("use text5 and text1");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("содержит данные");
        File file1 = new File(reader.readLine());
        System.out.println("пустой");
        File file2 = new File(reader.readLine());

        Scanner scanner = new Scanner(file1);
        FileWriter writer = new FileWriter(file2);

        boolean first = true;
        while (scanner.hasNext()){
            String [] line = scanner.nextLine().split(" ");
            String del = first ? "":", ";
            for (String s:line){
                if (s.length()%2==0){
                    writer.write(del + s.substring(0,1).toUpperCase() + s.substring(1));
                    first=false;
                }
            }
        }
        scanner.close();
        writer.close();
    }
}
