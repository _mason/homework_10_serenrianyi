package Main;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MinMaxBytes {

    public static void main(String[] args) throws IOException {

        Map<Byte, Integer> map = new HashMap<>();

        System.out.println("use text2");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        FileInputStream fis = new FileInputStream(reader.readLine());

        while (fis.available() > 0) {
            byte data = (byte) fis.read();
            if (map.containsKey(data)) {
                map.put(data, map.get(data) + 1);
            } else map.put(data, 1);
        }

        int min = Collections.min(map.values());
        int max = Collections.max(map.values());

        for (Map.Entry<Byte, Integer> tmp : map.entrySet()) {
            if (tmp.getValue() == max) System.out.println("Maximum= " + tmp.getKey());
            else if (tmp.getValue() == min) System.out.println("Minimum= " + tmp.getKey());
        }

        fis.close();
    }
}
