package Main;

import java.io.*;
import java.util.Scanner;

public class ReverseLines {

    public static void main(String[] args) throws IOException {

        System.out.println("use text5 and text1");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("содержит данные");
        File file1 = new File(reader.readLine());
        System.out.println("пустой");
        File file2 = new File(reader.readLine());

        Scanner scanner = new Scanner(file1);
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file2));

        while (scanner.hasNextLine()){
            StringBuilder done =  new StringBuilder(scanner.nextLine());
            fileWriter.write(done.reverse().toString());
            fileWriter.newLine();
        }
        scanner.close();
        fileWriter.close();
    }
}
