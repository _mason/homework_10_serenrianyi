package Main;

import java.io.*;

public class Task7 {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Person person = new Person("Ivan", "Egorov", Sex.MALE);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(person);

        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(baos.toByteArray()));
        Person person1 = (Person) ois.readObject();
        System.out.println(person1);

    }

    public static class Person implements Serializable {
        transient String firstName;
        transient String lastName;
        String fullName;
        final String finalString;
        Sex sex;
        transient PrintStream outputStream;

        Person(String firstName, String lastName, Sex sex) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.fullName = String.format("%s, %s", lastName, firstName);
            this.finalString = "final string";
            this.sex = sex;
            this.outputStream = System.out;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", fullName='" + fullName + '\'' +
                    ", finalString='" + finalString + '\'' +
                    ", sex=" + sex +
                    '}';
        }
    }
}
