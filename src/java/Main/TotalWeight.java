package Main;

import java.io.*;
import java.util.*;

public class TotalWeight {

    public static void main(String[] args) throws IOException {

        System.out.println("use text4");

        Map<String, Integer> map = new HashMap<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        File file = new File(reader.readLine());

        Scanner scanner = new Scanner(file);

        int max = 0;

        while (scanner.hasNextLine()){
            String name = scanner.next();
            int weight = scanner.nextInt();

            if (map.containsKey(name)) map.put(name, map.get(name)+weight);
            else map.put(name,weight);

            max= (map.get(name)>max) ? map.get(name) : max;
        }
        for (Map.Entry<String, Integer> tmp : map.entrySet()){
            if (tmp.getValue()==max) System.out.println(tmp.getKey() + " " + tmp.getValue());
        }
        scanner.close();
    }
}
