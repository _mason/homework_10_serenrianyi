package Main;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class SearchInFile {

    public static void main(String[] args) throws IOException {

        System.out.println("use text3");

        List<String> searchedLines = new LinkedList<>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        File file = new File(reader.readLine());

        Scanner scanner = new Scanner(file);

        String findWord = reader.readLine();

        while (scanner.hasNextLine()){
            String done = scanner.nextLine();
            if (done.contains(findWord)){
                searchedLines.add(done);
            }
        }
        scanner.close();
        System.out.println(searchedLines);
    }
}
